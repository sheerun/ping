var webpack = require('webpack')
var WebpackDevServer = require('webpack-dev-server')
var config = require('./webpack.config')

config.entry.unshift('webpack-dev-server/client?http://localhost:3000')

new WebpackDevServer(webpack(config), {
    publicPath: config.output.publicPath
  })
  .listen(3000, 'localhost', function (err, result) {
    if (err) {
      console.log(err)
    }

    console.log('Running at http://localhost:3000')
  })
