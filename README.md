# Ping!

Discover own location solely by pinging other servers

## Installation

Install node@4 and npm@3 and then:

```
npm install
npm start
```

You can create production build with:

```
npm run build
```

## LICENSE

MIT
